## Introduction

Binary files -> metadata and executable assembly
    can't inject compiled binary into other programs.
    have to just inject executable bytes, strip of headers.
   

at&t assembly syntax

gcc to compile -m32 flag
objdump to disassemble

isolate assembly to bytes. 

## Shellcode

- Instructions injected and the executed. usually a remote shell should be started. 
- spawn shell process. 
-
`gcc name.c -o name -fno-stack-protector -z execstack -no-pie -m32`

pass pointer to data not actual data. how do we get data there? 
shellcode should be self contained, no assumptions about vuln process, don't assume things are in memory where you would expect. 

### Problem: position of code in memory is not known. 

- Address space layout optimization-> address space is shuffled around, no static addr spaces.

- Shellcode should have relative addressing.
- Discovering relative address as we execute. 
    - call instruction (saves Instruction Pointer IP on the stack and jumps)
        - this is the pointer to return to when call is completed. 
        - jmp instruction at the beginning of shell code to call
	- call right before string
	- call jumps back to first instruction after jump 
	

execve (c function) executes another piece of code.

- int execve(char \*file, char \*argv[], char \*env[])
- name, address of null terminated array, {"/bin/sh", NULL} 
- env (environment variables in shell)


compile this piece of c code to get the shell code

Introduce in to memory space: addr, /bin/sh, 0

- move call number into eax
- address of string /bin/sh into ebx (per above)
- load address of address of /bin/sh into ecx using lea
- 

### More problems

- Shellcode is ususally copied to str buffer
- String copy is until you meet a NULL byte. Any null bytes would stop copying, must elminate null bytes.
- 

- can generate null byte by `xor reg, reg` instead of `mov 0x0, reg` 

- use a1 registers instead of eax. 



