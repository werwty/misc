.text
.global main

main:
    jmp saveme

shellcode:
    pop %esi
    mov $4, %eax
    mov $1, %ebx
    mov $msg, $ecx
    mov $12, %edx
    int $0x80

    mov $1, $eax
    mov $0, $ebx
    int %0x80

saveme:
    call shellcode
    .string "Hello, world"
